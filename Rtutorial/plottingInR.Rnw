%% Plotting in R
\documentclass[11pt]{report} %% May want to try twocolumn soon
\usepackage{myStyle}
\ProvidesPackage{myStyle}

\title{Plotting in R}

\author{Dr. Mark Peterson}
\date{}



\begin{document}
\chapter[Plotting in R]{Plotting in R\\ \Large Introduction to R for stats}

<<setup, include=FALSE, echo=FALSE, cache=FALSE,results="hide",warning=FALSE>>=
chapName <- 'plotInR'
  library(knitr)
# set global chunk options
opts_chunk$set(fig.path=paste('figure/',chapName,'-',sep=''),
               fig.align='center', fig.show='hold',tidy=FALSE)
options(replace.assign=TRUE,width=90)
hook_source = knit_hooks$get('source')
knit_hooks$set(source = function(x, options) {
  txt = hook_source(x, options)
  # extend the default source hook
  gsub('~', '\\\\mytilde', txt)
})
 setwd("~/Documents/Juniata/teaching/eukGenomics/lectures/labs")
@


\section{Background}

Up until this point, you have been doing your statistics by hand. This is a necessary starting point to understand the basic concepts, and is the best way to initially acquaint yourself with data analysis. However, as data sets grow larger and statistical questions grow more complex, being able to run your analyses in a computer program will become increasingly important. Today, we are going to explore one of these tools: R, a statistics environment designed to be a fully operational programming language. 

\subsection{Why R?}
R was designed with high quality graphics explicitly in mind,
which makes the default plots it produces elegant
while allowing users full control over the outputs.
%
In addition,
this flexibility extends to the handling and display of raw data,
making it possible to store, manipulate,
and analyze a wide variety of data types in a single program.
%
This flexibility comes at the cost of a large learning curve,
especially as many of you are probably new to computer programming.
%
Finally, R is free, both free as in beer (no cost)
and free as in speech 
(the source code is available,
can be manipulated, and can run on any platform),
making it an accessible choice for students and researchers.


Today, we will focus on some simple examples of data manipulation and graphing,
slowly building your repertoire and confidence.
%
RStudio provides a useful wrapper for R -
it allows us to save what we are doing each step of the way,
and displays useful information
(graphs, help files, etc.)
alongside our progress.
%
For all of our activities,
we will be working in RStudio.
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{What you will learn today} %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
\item How to install and utilize a statistical package
\item Be able to import data into R
\item Be able to graphically represent your data
\item How to comfortably approach command line and computer code problems
\end{itemize}

\section{Installing R and RStudio}

Because we all just installed Ubuntu,
I am going to pull the details for our OS here.
%
However, once you upgrade Ubuntu
(or if you want to install this on a different OS),
you can vist
\href{http://cran.r-project.org/bin}{http://cran.r-project.org/bin}
for more details on th installation of R and
\href{http://www.rstudio.com/ide/download/desktop}{http://www.rstudio.com/ide/download/desktop}
for information on RStudio.

\subsection{Install R}
First, we need to add the R repository to our file options.
%
Alternatively, we could download the source code and compile ourselves,
but that reduces some of the flexibility to make changes later.
%
To add the R source,
open a terminal window by holding ctrl-alt-t.
%
Next type:

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
sudo gedit /etc/apt/sources.list
\end{alltt}
\end{kframe}
\end{knitrout}

You will need to enter your root password 
(this is one of the strong protections of Linux,
and makes it difficult for malicious programs to affect other programs).
%
It will then open a text editor window,
scroll down to the bottom and add the following lines:
\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
## R source codes
deb http://cran.mirrors.hoobly.com/bin/linux/ubuntu/ precise/
deb http://archive.canonical.com/ precise partner
deb-src http://archive.canonical.com/ precise partner
\end{alltt}
\end{kframe}
\end{knitrout}

Save the file, and close the window.
%
Now, back at the terminal, run the following three commands:

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
sudo apt-get update
sudo apt-get install r-base
sudo apt-get install r-base-dev
\end{alltt}
\end{kframe}
\end{knitrout}


\subsection{Install RStudio}

Installing RStudio is a bit more straigth forward,
as we will be working directly from the relevant file.
%
As with R, visit the above url for installation on other systems.
%
That link is also where new releases of RStudio will be posted,
the links below are the most current version as of 2014-Jan-22.

In a terminal, run the following line:

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
sudo apt-get install http://download1.rstudio.org/rstudio-0.98.493-i386.deb
\end{alltt}
\end{kframe}
\end{knitrout}

Note, if you installed 64-bit, replace the `i386' with `amd64'.
%
Now, you should have R and RStudio installed,
and - you have just successfully run a computer
from a linux command line!

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Getting Started}
First, open RStudio. Then, select "File $\rightarrow $ New $\rightarrow $ R Script" to open a new document. In the top left corner. This document will hold all of our commands for the lesson. This will:

\begin{itemize}
\item Provide us with a record of everything we do
\item Allow assessment of your progress
\item Provide you access to previously used and learned commands
\item Allow us to fix small mistakes in long pieces of code without retyping everything
\item Give you something to turn in at the end of the lab
\end{itemize}
Now, save this R script file (Either "File $\rightarrow $ Save" or Ctrl+S) in your own folder begining with your last name. As we go along, make sure to save frequently - there is no such thing as saving too often. Now, lets start by making some notes to ourselves. In R, anything that is preceded by a "\#" is ignored. This lets us make comments, which can be incredibly helpful if we ever need to come back to the code, or share it with others. At the end of the lab, you will each submit your script, along with an additional file we will produce near the end of the lab.

A note on convention: every command I tell you to type will look like the box below. In general, you will just copy what you see below directly into your script file; however, things in ALL-CAPS should be replaced with your information.

<< tidy=FALSE,results="hide",warning=FALSE>>=
## Script written by FIRSTNAME LASTNAME
## Euk Genomics Class First Script
## YEAR-MON-DD
@

This information will help to remind you of what you were doing when writing the code,
what its purpose is/was,
and when it was written.
%
Believe me,
when you come back to this to work on your projects,
you \textit{will} appreciate having these comments.
%
Comments can also help others to understand your code,
and should become a habit for you.
%
These directions will include comments throughout,
but feel free to add your own notes to the code as well.
%
Part of the grade for this assignment will include your comments
and answers to questions in this document.


Now, we need to see how these commands are actually passed to R.
In RStudio, if you press Ctrl+Enter,
it will send the highlighted text
(or current line if nothing is highlighted)
to the command line.
%
Highlight the three lines of comments
(either with the mouse,
or by holding shift and using the arrow keys)
and press Ctrl+Enter.
%
This sends the comments to the Console and executes any code.
%
Alternatively,
you can click "Run" above the script window to accomplish the same thing.
%
Here, the text is commented, so nothing happens.

Next,
we need to tell R where it should be looking for your documents,
and where it should save outputs.
%
This is similar to opening a folder,
but we need to tell R in text.
%
This is called "setting the working directory",
and it will be the folder in which R opens and saves any files that you use,
until you change the directory.
%
In RStudio,
this can be done from a drop-down menu.
%
Click "Session" $\rightarrow $ "Set Working Directory"
$\rightarrow $ "To Source File Location."
%
This will set the directory to the folder in which your script file
(where you are typing commands) is located.
%
If you want to select a different directory,
click instead on "Choose Directory."

You will notice that a command showed up in the terminal window,
which should read something similar to:

<< ,results="hide",warning=FALSE,eval=FALSE,echo=TRUE, tidy=FALSE>>=
## Manually set working directory
## Make sure that this is set for your computer, not mine
setwd('~/Documents/eukGenomics/scripts/')
@

Copy this line into your script, near the very top;
however, I want you to remove the 'scripts' portion
and re-run the command so that you are working in the directory
above 'scripts.'
%
This will make it easier to load in your data,
which is in a different directory.
%
When you set the working directory,
always put it near the very top of your script.
%
That way, if you are working on a different computer
(or sharing your script) it is easy to see (and change)
the working directory to something that works for the local computer.

\section{Entering Simple Data}
The most important thing to be able to do in any stats program is to import your data.
%
In R, there are several methods for doing so,
and we will focus on just a few of them.
%
Variables in R are very flexible,
and can take many different forms.
%
Variable names must begin with a letter,
but then can be any length and can include numerals.
%
In R, an arrow, composed of a ``less than'' symbol and a dash (``<-'')
is used to assign variables. 
%
Try it with this simple example:

<<,results="hide",warning=FALSE>>=
## Store a simple variable, then display it
testVariable <- 3
testVariable
@

To execute the commands,
either highlight all the type Ctrl+Enter,
or place the cursor on the first line and press Ctrl+Enter for each line.
%
The first command creates a variable,
and the last line tells the console to display it.
%
This variable can now be used just like a number.
%
Try the code below, typing it in the script,
then using Ctrl+Enter to execute it:

<< ,results="hide",warning=FALSE>>=
## Use the variable
testVariable + 6
testVariable * 4
@

Now, go back and change the value of the variable by changing the line assigning the variable.
%
Re-execute the assignment and all of the lines using it.
%
Note that this can allow you to re-do an analysis if your initial values change
(e.g., because of a typo, or just adding observations)
without having to re-type all of your previous commands.

\section{Entering Longer Data}
However,
a single value like that is not usually very useful for statistics.
You will often have multiple observations, and will want to keep them all together, rather than saving each as a different variable. R may have already given you a hint that it can do this. When you displayed  the value of testVariable, you likely noticed a "[1]" next to the value. This is an "index" telling us that the value displayed is the first element of that variable. That is because, in R, most variables are stored as "vectors." This allows us to store many values in a single variable. We need to tell R that we are giving it several values to store together using the command "c()" Try it out with this code:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Make a test vector, then display it
testVector <- c(1,3,4,6,8,10,15)
testVector
@

Vectors in R have some special properties, and these properties will become very useful. First, lets see how vectors handle a little bit of arithmetic:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=

## See how vectors respond to arithmetic
testVector + 3
testVector * 2
testVector + testVector
@

What did you notice about the outputs?
%
For arguments that are only one number long,
the same procedure is applied to each element (item) of testVector.
%
When the arguments are the same length
(have the same number of stored values) as testVector (e.g., itself),
then the argument is applied element-for-element.

\section{Playing with Data}
Now that we have some sense about how these things work, we can begin to look at the data a little more closely. First, lets create a new vector with each element being 3 greater than the elements in testVector. Go back to the the first arithmetic line with testVector, change it to the below and execute:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
testVectorB <- testVector + 3
@

All outputs in R can be saved in this fashion, which allows you to go back and use them later or to perform other manipulations on them. For example, what if you only want to know what the 3rd element of testVectorB is? R allows us to do that using brackets "[ ]" to tell it which element we want to see. Try it:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display only the 3rd element of a vector
testVectorB[3]
@

We can also use this to display only elements that meet certain criteria. Let's display only the elements that are greater than 8:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display only elements greater than 8
testVectorB[testVectorB > 8]

## Show how it is done:
testVectorB > 8
@

As you can see from the last line, R produces a type of value called a "logical" that says whether or not a condition (here, being greater than 8) is met. When placed within brackets, only those elements that meet the condition are displayed. This is useful for displaying certain data, but is especially important when looking at two different variables. For example:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display the value of testVectorB when testVector equals 3
## Note that two '=' are used for logical tests
testVectorB[testVector == 3]
@

As we begin to analyze the data you have generated in class, we will have grouping variables (e.g., categories, such as light treatment) that can be used to split the data just like this.

Finally, we are going to generate a plot using these two vectors. The general format for plotting is to give the x-coordinates, followed by the y-coordinates, as follows:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Make a plot of two vectors
plot(testVector, testVectorB)
@

There are several additional options that we can use to change the plot, and we will explore those as we move forward with analyzing your data.

\section{Importing spreadsheet style data}
Typing in data, as described above, is great for small datasets.
What about when you have 100 observations,
each with multiple measurements and properties?
%
You could go through and type them all as individual vectors,
but that would get cumbersome rapidly,
and leads to typos and other errors affecting your results.
%
Instead,
we can use a type of variable called a data frame
(data.frame in R)
that will keep each vector together, and reduce errors.
%
Try this:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Make a data frame out of the two testVector's, then display it
testDataFrame <- data.frame(testVector,testVectorB)
testDataFrame
@

As you can see, the rows now show which elements of each vector correspond with each other. In this way, you can see the relationship between the two vectors, and, if one were a grouping factor (i.e., category), use that relationship in your analyses. However, this still involved typing in all of your data, which is still a pain and error-prone. For any large dataset, you probably already have a spreadsheet, and would like to be able to just use that.
%
A safe, universally compatible approach,
is to use a text based format for saving your data,
such as comma-separated values (csv).

To import data, go to "Tools" then "Import Dataset" and "From text File."
%
For today, the data are available
% in the P:drive under "Academic/petersm/biostats/compLab1/data" 
from the zipped folder you were emailed.
%
Select the file "fastPlant.csv" and follow the on-screen prompts.
Make sure to change the separator parameters if needed (e.g., to a comma),
and tell R that the dataset has a header row.
%
Use the preview of the Data Frame (in the bottom right of the window)
to make sure that the dataset is reading in correctly.
%
If necessary, name the resulting data.frame "fastPlant" in the top left.
%
Click import, then \emph{copy the resulting command into your script}.
%
It should look something like:

<< tidy=FALSE,results="hide",warning=FALSE,echo=TRUE>>=
## How you would load a data set from the command line
## Note that this is relative to your working directory.
## Your path may be different, and should be copied 
##   from the Console after going through the menus.
fastPlant <- read.csv("data/fastPlant.csv")
@

There are several ways to access the vectors in this data.frame, and we will explore a couple.
%
First, you can use the brackets ( " [ "  and" ] ") just like for vectors.
%
Here the format is "[row, column]." An empty argument means "include all" as you can see below:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display the third column, then the 4th row
fastPlant[,3]
fastPlant[4,]

## Display just the 4th element of the 3rd column
fastPlant[4,3]
@

This format works for several data types in R,
but data.frames allow the use of column names,
which makes the code much easier to follow.
%
To call a single column by name,
we use the "\$" symbol.
% 
This produces a vector,
which can then be manipulated in the same manner as the vectors we used earlier.

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display a column by name
fastPlant$Height

## Display the heights of only fertilized plants
fastPlant$Height[fastPlant$Fert == 'High']
@

This is a great way to work with multiple datasets, and keeps things very clean.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Plotting data using fast plant results}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
One of R's most powerful features is its plotting ability and options. The default options are a great place to start, and they can be modified readily to produce exactly the plots that you envision. To start, let's plot the height of each plant, split by whether or not it was fertilized. To do this we will use the operator "\textasciitilde{}" (a tilde, usually above the tab key) to denote that we are entering a formula. That is we are telling R to consider what we input as "Response Variable \textasciitilde{} Predicting Variable." Try the below:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Plot the heights of plants based on fertilization
plot(fastPlant$Height ~ fastPlant$Fert)
@

Now, look at the graph that was produced. It looks rather different than our earlier plot. For categorical data, R's default is to produce box plots. What if you want to plot the means and confidence instead? You could go through and compute the values yourself and plot those, but here we encounter another of R's powers: user created packages. These packages cover a huge range of things, and exist for nearly anything you might like to do. Here, we would like to use a function "plotmeans" from a package named "gplots" so we need to install the additional package. Type and run the following:
<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",echo=FALSE,message=FALSE,warning=FALSE>>=
library(gplots)
@

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",eval=FALSE>>=

## Install a new package, make sure to follow on screen prompts. 
install.packages('gplots')

## Load the package for use:
library(gplots)
@
Now, use one of the packages functions to make a new plot:
<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Use the plotmeans function from the new package
plotmeans(fastPlant$Height ~ fastPlant$Fert)
@

This plots the mean for each group, along with the confidence intervals. What if we don't want the line connecting the two groups? First, we can look at the help page for plotmeans, which will tell us what arguments we can use (and their default values). 

<< results="hide",warning=FALSE,eval=FALSE,tidy=FALSE>>=
## Open help for plotmeans
?plotmeans
@

Then, we can change an argument to avoid adding those lines.

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Re run with the parameter changed
plotmeans(fastPlant$Height ~ fastPlant$Fert, connect=FALSE)
@

This output looks a lot better and suggests something about the effect of fertilizer on growth. Add a comment (\#\# comment) that says what this figure suggests. However, there is still more that we can learn from plots like this. We actually have 4 treatment groups, and we can display them all at once, even though there is not currently a variable naming them explicitly. We could add such a variable, but there is actually a simpler workaround: the function "interaction()" Try the below code to plot the full experimental design:

<< tidy=FALSE,fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Plot the full experiment
plotmeans(fastPlant$Height ~ interaction(fastPlant$Fert,fastPlant$Light, sep='\n'),
          connect=FALSE)
@

This plots the four groups (2x2 interaction) separately, again with mean and standard error bars. What does this plot suggest about the treatment? Add a comment with your answer. A few notes are useful here. First, notice that we are able to use a function within a function to provide us with a different input - this is an important feature of R that can really help keep code readable. Second, note the "sep= '\textbackslash{}n' " in the function. This tells the program to separate the two factors (Fert and Light) with a newline. You can use any separator you would like, but the newline tends to work best for plotting like this. Finally, the plot is a bit messy still. You can control a number of parameters to clean this up. Open the help, then add a couple parameters to improve the plot. I will leave the specifics up to you:

<< tidy=FALSE,fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=

## Open the help, then make the plot prettier
?plotmeans
plotmeans(fastPlant$Height ~ interaction(fastPlant$Fert,fastPlant$Light,sep='\n'),
          connect=FALSE, main= 'TITLE FOR YOUR PLOT',
          xlab= 'Treatment Groups (Fert; Light)', ylab= 'Height (UNITS)')
@

Note: in this document, continued lines will be indented as above. In RStudio, the line can continue to the right indefinitely. If you would like to, you can add line breaks manually, and RStudio should indent appropriately. If you use multiple lines, remember that you need to run the full command, not just individual lines. Feel free to modify any other parameters that you would like as well. Just make sure that the plot looks nice when it is done. Before we move to the next dataset, lets save this plot. In the plot window, click on "Export" then "Save Plot as Image ..." Name the file with your last name, and save it.

\section{Looking at normality using penny ages}
Now, we are going to switch to a new data set, and play with a couple of other functions in R. For the sake of being able to find things, I like to separate out the sections of a script with something like:

<< results="hide",warning=FALSE,tidy=FALSE>>=
##################################################################
##################### PLAY WITH PENNY DATA #######################
##################################################################
@

This way, it is easy to search back through your code to find a particular section.
%
You could, alternatively, create a new script.
%
However, to keep the lab all together, let's not.
%
Now, we are going to read in the penny data,
just like we did with the fast plant data.
%
Either,
go to "Tools" then "Import Dataset" and "From text File"
or try typing it directly, as listed below.
%
Note that in RStudio,
hitting `tab' will auto-complete many commands,
variable name, and even the path to files.
%
If you use the menus,
make sure to change parameters (including setting the "heading" to "yes"),
and name the resulting variable,
this time "pennyData"
%
Don't forget to copy the resulting command into your script.
%
From here, we can begin using the data.

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Load pennyData
pennyData <- read.csv("data/pennyData.csv")

## Display some of the data
pennyData$Penny.Ages
@

As you can see, there are roughly 1000 penny ages in the data set, but it is difficult to get a sense for what the data look like from 1000 numbers, particularly if they weren't ordered. A histogram is a great tool to display the shape of a dataset. Here, we are going to search for the function within RStudio, but you could easily type "make histogram in R" into Google for some more great examples. 
%
Alternatively, you can search directly in R by using
``??histogram'' at the command line.

Look through the resulting list, to see if anything looks promising. It seems that "plot.histogram" might be useful. Click on that and look at the resulting help document. This doesn't look quite right, but does point us to a function called "hist." Click on one of the links to "hist" and browse through that help file. That seems more like it. Now, let's try using hist():

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Plot a histogram
hist(pennyData$Penny.Ages)
@

Does the data look normally distributed to your eye? Add a comment to your script describing the graph and what it suggests about the distribution. Now, let's try a plot that will give us a better idea about the normality: the qqnorm() function creates a plot with the observed data (y-axis, by default) against the expected values from a perfect normal distribution.

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Plot the qq-plot for penny ages
qqnorm(pennyData$Penny.Ages)

# Add a line to observe deviation
qqline(pennyData$Penny.Ages, col='red')
@


Does the data align very closely to the line? Add a comment suggesting whether or not you think the data are normal based on this plot. Note here that the argument "col=" lets you set the color of the output. This argument works for many plot functions, and there are 657 named colors (type "colors()" for a list), or can be specified with hexadecimal notation (e.g 'FF0000'). 

For comparison, we can generate a random data set from the normal distribution, and see what the plot looks like. Try this:

<< fig.width=4, fig.height=4, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Generate random data to see what a QQ-plot 'should' look like
## generates 1000 random data points from the normal distribution
randData <- rnorm(1000) 

qqnorm(randData)
qqline(randData,col='red')
@

Use that graph as a baseline against which to assess your data. You can use the "Back" button (left facing blue arrow) above the plots to view previously created plots. Update your comment as needed, given your new information. In many cases, this eye-ball approach is a great first approximation of the normality of your data. However, R also has several built in normality tests, which we will explore next lab.

\section{A few more plotting examples using hot dog data}
We are now switching to another data set again, this time a data set on the calories and sodium in various types of hotdogs, originally collected by Consumer Reports in June 1986 (pp. 366-367). These data are in the file "hotdogs.csv" As before, put in a new separator (e.g. lots of "\#"s) to make it easy to find this section if needed. Load the data as we have above (look back if you need to), name the data.frame "hotdogs". Don't forget to copy the command into your script or type it directly.

<< echo=FALSE,results="hide",warning=FALSE>>=
hotdogs <- read.csv("data/hotdogs.csv")
@

Next, lets play with a few more plotting options. This time, we want to look at the relationship between sodium and calories. So, try:

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Plot relationship between Calories and Sodium
plot(hotdogs$Calories, hotdogs$Sodium)
@

This plot works, but it is missing a title and information about units. Use the plotting parameters from above (e.g., main, xlab, ylab, etc) to modify the plot to make it clearer. Does it appear that sodium is related to calories? Add a comment to your script.

What if, however, only some types of hot dogs show such a relationship? We can color code the types of hot dogs to look at them separately. The simplest way to do this is with the function points() that adds points to a graph. Recall from earlier that "[ ]" calls specific elements from a vector, and can be used with a logical test to limit which points are displayed. Here, we will also use a different shape for the points that will be easier to see. Look at the help for par (?par) for more information about all graphing parameters, including pch. 

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Add color coded points for each type of hot dog
## Feel free to use different colors, or use '?par' to look at other 'pch' options
## the '==' tests to see if the values are identical
plot(hotdogs$Calories, hotdogs$Sodium)

points(hotdogs$Calories[hotdogs$Type=='Beef'],
       hotdogs$Sodium[hotdogs$Type=='Beef'], pch=19,col='red')

points(hotdogs$Calories[hotdogs$Type=='Poultry'],
       hotdogs$Sodium[hotdogs$Type=='Poultry'], pch=19,col='yellow')

points(hotdogs$Calories[hotdogs$Type=='Meat'],
       hotdogs$Sodium[hotdogs$Type=='Meat'], pch=19,col='blue')

@

Now, each of the types is color-coded, and we can interpret each. Has your conclusion changed at all? Add a comment to your script here. Finally, we can add a legend to the plot with the function legend().

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE,eval=FALSE>>=
## Add a legend
## 
## The x and y coordinates position the top-left corner of the graph, change
## them as needed
legend("bottomright", inset=.05)
       legend=c('Beef','Poultry','Meat'),
       fill=c('red', 'yellow','blue'),title='Type')
@

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE,echo=FALSE>>=
##Actually make the plot for above
plot(hotdogs$Calories, hotdogs$Sodium)
points(hotdogs$Calories[hotdogs$Type=='Beef'],
       hotdogs$Sodium[hotdogs$Type=='Beef'], pch=19,col='red')
points(hotdogs$Calories[hotdogs$Type=='Poultry'],
       hotdogs$Sodium[hotdogs$Type=='Poultry'], pch=19,col='yellow')
points(hotdogs$Calories[hotdogs$Type=='Meat'],
       hotdogs$Sodium[hotdogs$Type=='Meat'], pch=19,col='blue')
legend("bottomright",legend=c('Beef','Poultry','Meat'),
       fill=c('red', 'yellow','blue'),title='Type',
       inset=.05)
@


\section{Bar plots and tables using violations data}
Finally, we will work on a new type of plot in R: the bar plot. For this, we will use the violations data set. These data are a sample of the fines assessed to college students on a large college campus, including the Year of the student (e.g., Freshman or Senior), and the amount of the fine. The data are in the file "violations.csv"

Load it as before.
Don't forget to copy the commands into your script,
if you use the file menu.
%
The year data is of the class (aka type) "factor." This is a variable type that R uses to recognize that the data are grouping variables -- categories that separate data. The values of these groups are by default stored alphabetically as the "levels" of the variable. The first thing we want to do is change that order to something more useful for our purposes:
<< echo=FALSE,results="hide",warning=FALSE>>=
violations <- read.csv("data/violations.csv")
@

<< results="hide",warning=FALSE>>=
## See what kind of variable 'Year' is
class(violations$Year)

## View the levels of the variable 'Year'
levels(violations$Year)

## Set the levels of Year to the normal order
violations$Year <- factor(violations$Year,c('Fr','So','Jr','Sr'))
@

Now, lets look at a helpful summary of the data. For this, we will use the function table() in R. table() counts the number of times a given value occurs, and produces and output that shows those counts.

<< results="hide",warning=FALSE>>=
## Produce a simple table, here showing the number from each class
table(violations$Year)

## Save and display a more useful table
violationsTable <- table(violations$Fine,violations$Year)
violationsTable
@

This table shows the number of fines of each value given to each class in the dataset. Such a table can then be used for a large number of other R functions, including plotting using barplot(). A simple example is just to call:

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Make a simple bar plot of vioations
barplot(violationsTable)
@

This simple plot can give us a good first look at the data, but we can make it more beautiful (one of the powers of R).

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Set some colors to use, Juniata colors seemed fitting
barColors <- c('blue','gold3','white','grey')

## Use the bar colors, and add a title to the graph
barplot(violationsTable, col=barColors, main='Plot of Fines by Year')

## Add a legend, again, move the x and y to a good place
legend("bottomright",legend=rownames(violationsTable),fill=barColors,bg="white",inset=0.05)
@

In addition, the barplot() function allows us to show the data next to each other, rather than stacked. This allows each of the fine values to be compared more easily, and may be more useful, depending on your purposes.

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Create a plot with the values side-by-side
barplot(violationsTable, beside=TRUE, col=barColors, main='Plot of Fines by Year')

## Add a legend, again, move the x and y to a good place
legend("topright",legend=rownames(violationsTable),fill=barColors,inset=0.05)
@

Finally, tables can be rotated to allow us to look at the data in a different way. This is the function t(), which transposes any table.

<< fig.width=8, fig.height=6, out.width='.4\\linewidth',results="hide",warning=FALSE>>=
## Display the rotated table
t(violationsTable)

## Make a plot of the transposed table
barplot(t(violationsTable),col=barColors,
        beside=TRUE,main='Plot of fines by cost')

## Add a legend, again, move the x and y to a good place
legend("topright",legend=rownames(t(violationsTable)),fill=barColors,inset=0.05)
@

As you have hopefully seen, R has an incredible amount of power. Today, we have focused largely on importing data and displaying it. In the future, use these notes, and the script you generated as a starting point for your analyses. Google is your friend (GIYF) as you move forward and add additional approaches and statistical tests.

\section{Assignment -- due before class Wednesday}

For your assignment today, you will need to turn in your script (which you have been generating all along) and an html output from the script to confirm that your code worked. RStudio has a built in tool to generate these files, but it requires an R package called knitr. Install the knitr package from the command line, not in your script using:
<< eval=FALSE>>=
install.packages('knitr')
@
\ldots and follow the on screen prompts.
%
Now, click on the small notebook icon just above your script.
%
It should say "Compile an HTML notebook from the current R script"
when you mouse over it.
%
Assign an appropriate title,
and put your name as the author.
%
Click "Compile" and an html file will be created.
%
Email the created html file to me (\href{mailto:petersm@juniata.edu}{petersm@juniata.edu})
with the subject line including: ``Euk genomics R lab 1''.
%
If you run into problems as you work on this,
email me with as much detail as possible about the problem.

\end{document}