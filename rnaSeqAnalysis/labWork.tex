%% Chapter on QC and processing

\chapter{Lab Work}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Most of the work for next-generation sequencing,
especially RNA-seq,
comes after all of the sequencing is done
and centers around four repeated letters in the computer.
%
However, to get to that point 
(where we have spent the rest of this manual),
many laboratory steps must be completed.
%
These steps are not, generally, difficult,
but they require a great deal of caution as the finished product
(RNA in this case) is often unstable and subject to contamination.


Here, we will walk through the basics of the laboratory steps
required for RNA-seq:
RNA-extraction,
rRNA removal (if necessary),
library preparation,
and sequencing.
%
Often several of the steps may be performed by a core facility for a fee.
%
This fee is often a convenience fee,
though it reduces the need to develop substantial infrastructure
for individual investigators,
and reduces the likelihood of errors caused by doing something for the first time.


This module will walk through the basics of the lab work necessary for RNA-seq analysis.
%
It will not, however,
go into great detail about any of these steps.
%
The simplest approach to each of these steps is to
buy a kit and follow those specific directions as exactly as possible.
%
There are a number of less expensive routes,
that involve a series of chemicals rather than proprietary kits.
%
These are often much easier to scale for different volumes or applications,
but require a bit more up-front expenditure on infrastructure.
%
For this year's workshop, 
we are lucky enough to be using a protocol,
developed by Dr. Arthur Hunt,
to make in-expensive librarires ourselves.
%
This protocol will be distributed separately.


\begin{figure}[h]
\begin{center}
\includegraphics[width=.35\textwidth]{../images/xkcd_trimester_699}
\caption{\href{http://xkcd.com/699}{xkcd.com/699/}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Learn how to extract RNA
 \item Learn how to prepare a sequencing library
 \item Make informed decisions about lab techniques and kits
 \item Understand the process that goes into lab work for RNA-seq
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to communicate and collaborate with other disciplines
%  \item Ability to apply the process of science
% \end{itemize}
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sample collection} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This process is entirely system, species, and question dependent.
%
There are, however, a few standard rules-of-thumb.
%
First, be fast.
%
Whatever technique you are using,
you want it to affect your tissue/cells as little as possible.
%
Therefore, move quickly but not in haste.
%
However, some studies suggest that animal tissues can be
processed several hours after death (Cheviron et al., 2011).
%
Second, get things cold (fast).
%
Dry ice, liquid nitrogen,
or a -80 C freezer will stop much of the RNA degradation,
and can often be used as a preservative even without any sort of buffer
(depending on your system).
%
Some alternatives rely on chemical preservation rather than cold temperature.
%
Finally, be consistent between samples.
%
One of the biggest worries is accidentally
(or by design)
treating samples from experimental groups differently,
which could easily cause false differences to be detected.
%
With consistency,
there is some leeway from the first two rules.
%
Even if there is some RNA loss,
if it is consistent across groups,
the stats should catch any problems.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{RNA extraction} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RNA extraction for RNA-seq is essentially identical to
RNA extraction for other applications,
such as microarray processing.
%
Everyone has a preferred method of extraction,
generally a kit designed for their species,
and most of these methods appear to yield relatively similar results.
%
The basic concept is to rupture the cell,
releasing RNA (and other cell contents, such as DNA and protein)
in a solution that will protect the RNA from degradation.
%
Most kits are designed to only collect RNA
(the RNA sticks to a filter, and is later removed - ``eluted'')
after it is cleaned.
%
Some applications,
such as phenol-chloroform extraction with TRIzol,
are designed to allow for the collection of
genomic DNA, protein, and RNA all a the same time (if desired).
%
This is accomplished by separating the cellular contents into
layers that can then be collected,
rather than relying on a single filter.
%
A detailed protocol for this is available from me if you are interested.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{rRNA removal} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



Ribosomal RNA (rRNA) makes up around 80\% of cellular RNA,
depending on species and estimate.
%
Unfortunately, this means that a large portion of RNA sequenced will be rRNA,
which makes it much more difficult to identify the coding sequences of interest.
%
For eukaryotic samples,
rRNA removal can be skipped if the library preparation
(see next section) uses poly-a isolation.
%
For prokaryotic samples, however,
it is necessary to remove rRNA before library prep,
unless they are of specific interest to the project.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Library Preparation} %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The final step before sequencing is library preparation.
%
RNA must be converted to cDNA and ligated to adapters for sequencing.
%
For most preparations,
this will involve breaking the material into smaller pieces
and selecting a very specific size fraction.
%
For paired-end sequencing,
this selection is what allows you to know how much space lies
between the two sequence pairs -- a necessity for accurate assembly and analysis.


Library preparation can require a substantial investment in infrastructure
(largely for breaking and size-selecting the RNA and having primers on hand)
as well as attention to detail that may be difficult to achieve for
students or first - time investigators without substantial oversight.
%
Depending on your available
time, infrastructure, guidance, expertise, and money,
it is often simpler (and more reliable) and occasionally less expensive,
to pay for a core laboratory to perform this step.
%
This reduces errors and allows them to use their infrastructure
(often including robots)
to reduce time and individual investigatory investment in equipment.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The best source for help on lab work is the kit directions and manufacturers themselves.
%
In addition,
many common questions have been asked and answered in online forums such as seqanswers.com.
%
A bit of searching may turn up an answer,
or at least direct you to somebody (or a forum) that may be able to answer your questions.

