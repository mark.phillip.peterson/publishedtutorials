%% Chapter on QC and processing

\chapter{Sequence processing and quality control}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
After sending off your RNA for sequencing 
(or downloading it from SRA)
and waiting patiently,
you finally have data!
\ldots
Now what?
%
The files you have received contain billions of bases of sequence
with information on quality,
but where to begin?
%
This module is the first step in that processing,
and the first time we will be working directly at
the command line interface with our data.
%
Next generation sequencing platforms,
by virtue of their large outputs,
are bound to produce some errors.
%
The goal of this module is to identify and remove many of these errors.

\begin{figure}[h]
\begin{center}
% \includegraphics[width=.9\textwidth]{../images/dilbertQC_bw}
\caption{\href{http://dilbert.com/strips/comic/2008-05-10/}{dilbert.com/strips/comic/2008-05-10/}}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Learn about sequencing quality scores
 \item Learn how to identify potential problems in sequencing data
 \item Learn how to handle and remove these potential errors
 \item Understand the indicators of quality sequence data
 \item Manage a large dataset without terror
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to apply the process of science
%  \item Ability to use quantitative reasoning
%  \item Ability to use modeling and simulation
%  \item Ability to tap into the interdisciplinary nature of science
% \end{itemize}
% 

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Checking data quality}  %%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

The first step of the process of data filtering is simply to see what you have.
%
The data are provided to us in a format called ``fastq'' that
includes a quality score,
which provides information on how confident the sequencer is in its base calls.
%
We will talk about this more in class,
but the basic idea is that for Illumina runs, the score (Q) is calculated as:

\begin{equation}
Q = -10 * log_{10} P
\end{equation}

Where `$P$' is the probability that the base call is incorrect.
%
Thus, a larger Q score indicates greater confidence in the base call.
%
These scores are encoded along with the sequence information using
the ASCII values corresponding to various characters
with some offset to avoid the non-printing characters
at the low end of ASCII values.
%
As a general rule,
scores over 20 suggest high quality data,
while lower scores may be a cause for some concern.
%
The steps below will walk us through how to identify those scores in a useful way.

This, like most of the linux commandline steps,
will occur on the computer cluster,
so connect, usign \verb;ssh;,
and let's begin
%
First, lets just look at one of the sequence files.
%
Move to the project directory, then use the command \verb;less; to display the sequence file.
%
Can you interpret those quality scores?
%
Neither can I, so we will need to use some programs to help us visualize it.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Setting up fastqc}  %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:QCsetUpFQ}
Running this from the command line the first time takes a few extra steps.
%
Primarily because both java and fastqc will need to be installed.
%
Both are slightly odd programs in that they do not require an ``installation''
but only require loading the program
and adding a few variables to call them.
%
First, we will install fastqc.
%
All software that we download will be added to a directory
named ``opt'' (for optional),
which we must create.


\begin{knitrout}\begin{kframe}\begin{alltt}
mkdir \mytilde{}/opt
\end{alltt}\end{kframe}\end{knitrout}

Now, we will move into that directory
and download the code for fastqc.
%
We will then unpack (de-compress) it
and delete the zip file.
%
Finally, we need to set the permissions on the file
to allow us to run the program.


\emph{Please} note here that \verb;rm;
works rather differently than you may be used to.
%
There is no ``Trash'' and there is no going back.
%
Once you delete something with \verb;rm;
it is \emph{gone}.
%
So, it is generally good practice to use the ``-i'' flag,
which stands for ``interactive''
and will ask you to confirm that you want to delete a file.

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \mytilde{}/opt
wget http://www.bioinformatics.babraham.ac.uk/projects/fastqc/fastqc_v0.11.5.zip
unzip fastqc_v0.11.5.zip
rm -i fastqc_v0.11.5.zip

cd FastQC
chmod u+x fastqc
\end{alltt}\end{kframe}\end{knitrout}

For java, copy the file ``jre-7u51-linux-i586.tar.gz'' from the shared directory
into `opt'.
%
You will then extract the file and remove the tar file.
We will then unpack (de-compress) it
and delete the zip file.
%
Remember that tab autocompletion is a huge time and head-ache saver.

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \mytilde/opt
cp \dataDirectory{}/jre-7u51-linux-i586.tar.gz .
tar -zxvf jre-7u51-linux-i586.tar.gz
rm jre-7u51-linux-i586.tar.gz 
\end{alltt}\end{kframe}\end{knitrout}




%% This doesn't work because, for some fucking reason
%%   loading java this way makes it run out of memory.
% To install java,
% we are going to load the programs as a module.
% %
% This is a method that is slightly different than we used before, and
% its specific implementation will depend on the system you are working in.
% %
% The basic idea is that updates to shared software
% (software available to all users)
% will be implemented more seamlessly this way.
% %
% It is not available on all systems,
% but is a nice feature of Mason,
% as it will automatically load the program,
% and add it to our path,
% for us.
% %
% As before, we are going to add the command to a profile
% to ensure that they are loaded every time instead of
% manually loading each time we sign in (or run a script).
% %
% 
% \begin{knitrout}\begin{kframe}\begin{alltt}
% module load java/1.7.0_51
% echo 'module load java/1.7.0_51' >> \mytilde/.modules
% \end{alltt}\end{kframe}\end{knitrout}



Now, we need to make it easier to run the program.
%
Right now, it will only run if we type the full pathway, which is a pain.
%
Instead, we are going to add the path to an ``Environmental Variable''
named PATH that tells the computer where to look for programs that you call.
%
Importantly, it searches these directories in order,
so if you have two identically named programs,
it will only call the first one it finds.
%
This also means we need to be careful not to accidentally remove part of the path.
%
The easiest way to do this is to add the paths that we want to our variable,
without deleting anything else.
%


\begin{knitrout}\begin{kframe}\begin{alltt}
export PATH=\$PATH:\$HOME/opt/FastQC:\$HOME/opt/jre1.7.0_51/bin
echo 'export PATH=\$PATH:\$HOME/opt/FastQC:\$HOME/opt/jre1.7.0_51/bin'  >>  \mytilde/.bashrc
\end{alltt}\end{kframe}\end{knitrout}

The first line adds fastqc and java to our path for this session,
but that will be forgotten when we next log out.
%
The second line adds that command to a file named ``.bashrc'' in your home directory,
which is run every time you log on.
%
Thus, from now on, FastQC and java will be in your PATH.





%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Running FastQC} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now, we are ready to analyze our data.
%
Move into the `seqData' directory in the project folder,
this will keep all of our results together,
instead of each running it in our own home folder.
%
You will each be assigned to a sequence file(s),
which will be yours to take all the way through this analysis.
%
Each of you will execute the following,
substituting your assigned file for \verb;fileName;
(note that throughout this document,
purple text like this should be replaced with your information).

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \dataDirectory{}/seqData
fastqc -o fastQCresults \hlnum{fileName}
\end{alltt}\end{kframe}\end{knitrout}

For each assigned file.
%
Once this has completed,
it will place a folder named ``fileName\_fastqc''
and a zipped version into the fastQCresults directory
within seqData.
%
From a terminal window, download the directory via \verb;scp;
by using the commands discussed in the ``Computer Setup'' module (See chapter \ref{chap:computerSetup})
(Windows users, recall the workaround required
from section \ref{sect:SetupLoad}).
%
Note the period at the end of the line,
which tells the computer ``save the file right here
with the same name.
%
You can replace the ''.`` with a different name for the file,
or even a path to a different location,
if you want.

\begin{knitrout}\begin{kframe}\begin{alltt}
scp -r \hlnum{user}@\computerAddress{}:\textbackslash
\dataDirectory{}/seqData/fastQCresults/\hlnum{file}_fastqc  .
\end{alltt}\end{kframe}\end{knitrout}

A few notes -- you can leave the full scp line on one line, 
but it does not fit across the page here.
%
If it is on one line, make sure that there are no spaces between the colon
and the start of your path.
%
Similarly, if you do leave it on two lines,
make sure that there are no spaces between the colon and backslash (''\") 
or on the new line before the start of your path.


You can then open the results in a web browser
by opening the directory
and clicking on the file ``fastqc\_report.html''.
%
Look at the file, and see if there is anything you think needs to be done to clean up the data.
%
Compare the results to others around you,
and we will discuss them as a group.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Trimming bad data} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
There are two ways of filtering data:
trimming ends that may have very low quality,
or removing reads that are low quality.
%
In general,
short-read sequence aligners take quality information into account,
and so conservative trimming and filtering is not necessary.
%
However, if you have a run with very low quality ends,
trimming those ends can help your analysis,
especially if you are assembly a \textit{de novo} transcriptome
(see chapter \ref{chap:deNovoAssembly} for more info).
%
This program suite will likely be used later,
and you will want it installed.


There are a number of tools designed to help you control read quality,
each with their own benefits.
%
For today, we will use a program called 'Trimmomatic'
because it does a great job of explicitly handling
paired-end data like these.
%
This program requires java,
which we already loaded,
but follow those directions if you need to start from this point.
%
Set up the program with the following:

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \mytilde{}/opt/
wget http://www.usadellab.org/cms/uploads/supplementary/Trimmomatic/Trimmomatic-0.36.zip
unzip Trimmomatic-0.36.zip
rm -i Trimmomatic-0.36.zip
\end{alltt}\end{kframe}\end{knitrout}

To call this, we will use java, and simply pass the arguments we want to use.
%
For more detail on each option,
go to the website:
\url{http://www.usadellab.org/cms/?page=trimmomatic}.
%
One note: paired-end data requires two outputs for each file,
one for those that match the opposite direction read,
and one for those that don't.
%
The code below is an example that may be a helpful starting point;
note that the `\textbackslash' at the end of each line means
`put this all on one line; don't hit return yet'
and can either be copied in directly (and interpreted by the console),
or omitted to put everything on one line (interpreted by you).


Do \emph{not} run this directly from the command line
on the sequence files we have been using.
%
The files we are using are large enough that you need to use
a different system (a script submitted via \verb;qsub;),
which we will introduce in the next chapter
(see section \ref{sect:MappingRun}).
%
If you would really like to use this,
make sure to run the \verb;head; command first to create
a sample subset of the data
(or, later, submit it as a script via \verb;qsub;).


\begin{knitrout}\begin{kframe}\begin{alltt}
cd \dataDirectory{}/seqData

head -n 4000 \hlnum{file}.fq > smallExamples/subset_\hlnum{file}.fq

cd smallExamples

java -jar \mytilde/opt/Trimmomatic-0.36/trimmomatic-0.36.jar \textbackslash 
     SE -phred64 subset_\hlnum{file}.fq\textbackslash
     trim_\hlnum{file}.fq \textbackslash
     CROP:85 HEADCROP:4 \textbackslash
     LEADING:3 TRAILING:3 \textbackslash
     SLIDINGWINDOW:4:15 MINLEN:30
\end{alltt}\end{kframe}\end{knitrout}

By line, these commands:
\begin{itemize}
 \item Call the program
 \item The input data and information
 \begin{itemize}
  \item `SE' for single end data
  \item the offset of the quality scores
  \item the input sequence files
 \end{itemize}
 \item The output trimmed sequence file
 \item Tell the program to keep the first 85 bases then cut the first 4 bases
 \item Cut the first (then last) bases, if the quality is below 3
 \item Check windows of 4 bases and cut the sequence when the average score goes below 15; only keep reads that are at least 30 bases long
\end{itemize}

We could then use these outputs in our downstream analyses.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Most, if not all,
of the programs that we use here have good webpages
with substantial information on the usage of their programs (linked below).
%
In addition,
sites like ``seqAnswers'' and ``BioStars'' address
many of the common issues that users encounter.
%
Searches for these sites (or just general questions)
will often lead you to individuals that have already addressed,
and solved, the same challenges that you are facing.
%


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
More information related to these topics can be found in:
\begin{itemize}
 \item \url{http://www.usadellab.org/cms/?page=trimmomatic}.
%  \item \url{http://hannonlab.cshl.edu/fastx_toolkit/}
 \item \url{http://www.bioinformatics.babraham.ac.uk/projects/fastqc/}
\end{itemize}


