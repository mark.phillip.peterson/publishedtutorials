%% Chapter on QC and processing

\chapter{Differential Expression}
\label{chap:DE}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Now that the reads have been quality checked, assembled, and aligned,
there is finally a bit of biology that can be directly addressed.
%
In this module,
we will start by examining differences in gene expression between our sample groups.
%
The analysis today will use just one of the programs currently available
to assess gene expression differences
(and more are constantly emerging).
%
We will discuss a few of the fundamental principle of gene expression
and use this program as an example.
%
However,
you may find that you like the interface (or assumptions)
of a different program better.
%
There are plenty of options available.


The commonality to all of these approaches is that
they all attempt some form of normalizing for read count
(we accomplished this once already by calculating fpkm along with raw read counts)
and then use a combination of modeling and basic statistics
to determine if two groups differ in expression.
%
This raises a very large problem of multiple testing:
with a p-value of 0.05,
we expect 5 in every 100 tests to be false positives.
%
Obviously, when dealing with thousands of genes,
that number overwhelms the real results.
%
There are a number of proposed corrections
(choosing the top X most-significant genes a priori,
setting a stricter p-value a priori, etc.),
but we are going to focus today on the False Discovery Rate (FDR).
%
FDR calculates an acceptable rate of false\-/\ to true\-/positives in a data set,
rather than relying on raw p-values.
%
It uses the p-values strike this balance,
usually at 0.05,
or 1 in 20 positives expected to be false-positives.




\begin{figure}[h]
\begin{center}
\includegraphics[width=.38\textwidth]{../images/xkcd_bayes_1132}
\caption{\href{http://xkcd.com/1132}{xkcd.com/1132}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}

\begin{figure}[p]
\begin{center}
\includegraphics[height=.92\textheight]{../images/xkcd_significant_882_bw}
\caption{\href{http://xkcd.com/882}{xkcd.com/882}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Discuss the concepts behind gene expression analysis
 \item Discuss statistical balance between false\-/positives and false\-/rejection
 \item Learn the basics of (at least) one differential expression analysis program
 \item Understand the basic principles managing large datasets
 \item Understand the statistical principals of large datasets
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to apply the process of science
%  \item Ability to use quantitative reasoning
%  \item Ability to use modeling and simulation
% \end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prepare R} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This chapter will utilize the R statistical environment to
conduct our analyses.
%
R is an incredibly powerful statistics package
designed with high quality graphics explicitly in mind,
which makes the default plots it produces elegant
while allowing users full control over the outputs.
%
In addition,
this flexibility extends to the handling and display of raw data,
making it possible to store, manipulate,
and analyze a wide variety of data types in a single program.
%
This flexibility comes at the cost of a large learning curve,
especially as many of you are probably new to computer programming.
%
Finally, R is free, both free as in beer (no cost)
and free as in speech 
(the source code is available,
can be manipulated, and can run on any platform),
making it an accessible choice for students and researchers.


If you have not used R before,
please consult an R tutorial before proceeding,
such as the one provided with this document.
%
Without it, you will still likely be able to complete this section,
but you will struggle to understand the syntax that is being presented.
%
At the least, you will need to install R,
available, with instructions,
at: 
\href{http://cran.r-project.org/bin}{http://cran.r-project.org/bin}
%
In addition, it is also strongly recommended that you install the graphical
interface RStudio to make R more comfortable to use,
available at:
\href{http://www.rstudio.com/ide/download/desktop}{http://www.rstudio.com/ide/download/desktop}.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Download data} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

A note to users looking to run this type of analyis on a complete dataset.
%
You will have noticed that,
to this point,
we have been analyzing each of the sequence read files separately.
%
This is great for learning what is happening,
but is not so great when you are trying to analyze
a full experiment,
which may have anywhere from 6 to 100 samples easily.
%
There are, however, much faster ways to analyze multiple samples,
in parallel,
though they are outside the scope of this manual.
%
For an example of such scripts, using these data,
please see the git repository
\url{https://bitbucket.org/petersmp/publishedtutorials/}.


Many of the tools for differential expression can be run
directly from the command line,
including the steps that we are going to run below.
%
However, it is often easier,
especially when first running analyses,
to run them on your local machine.
%
Here, we will download the count data,
and start running some basic analyses
on differential expression.
%
To do this,
first move all of the results of interest
(here, the ``genes.results'' files)
into a single directory,
to make the download easier.
%
Because we will be analyzing all of the samples,
this only needs to be run once,
not by each participant.

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Only one student should run this:}}
cd \dataDirectory{}/seqData
mkdir geneResult
cp align/*genes* geneResult/
\end{alltt}\end{kframe}\end{knitrout}

This copies all of the files containing the word ``genes''
from the ``align'' directory into our newly created geneResult'' directory`.
%
Now, on your local machine,
move (\verb;cd;) to a directory for this workshop,
then download that entire directory, and 
the annotation information with:

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {On your local machine:}}
cd \hlnum{/path/to/your/directory/}
scp -r \hlnum{user}@\computerAddress{}:\dataDirectory{}/seqData/geneResult .
scp -r \hlnum{user}@\computerAddress{}:\dataDirectory{}/seqData/annotations .
\end{alltt}\end{kframe}\end{knitrout}

Which will download our ``geneResult'' and ``annotations'' directories to where you are
(note the ``.'' which again tells \verb;scp; ``right here'').


%% Cut RSEM section

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{DESeq} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Several alternatives exist,
including other command line options,
such as RSEM, cuffdiff,
and a number of packages in R, including
limma,
cummeRbund,
EBSeq, and
DESeq.
%
In this section,
we will focus on the R Bioconductor package DESeq,
%% Links?
which is fast, flexible, and which I have used extensively.
%
DESeq utilizes a negative binomial distribution
and offers several alternative approaches for significance testing.
%
Covering the theoretical background of this package,
especially in contrast to others,
is beyond the scope of this workshop.
%
Instead,
please refer to the publication describing DESeq
and comparing it to other methods
(see section \ref{sect:DEfurther}).

\needspace{4em}
The general steps of the DESeq pipeline are:
\begin{itemize}
 \item Combine count data (such as from RSEM) into a single file
 \item Normalize the read counts to account for sequencing depth differences
 \item Estimate variance (called dispersion) for each gene
 \item Compare expression between groups, and
 \item Calculate significance
\end{itemize}

Each of these steps are relatively common,
though there is some discussion over the best way to handle
dispersion and significance.
%
For a more detailed description of these steps,
and the underlying theory,
please refer to the vignette for DESeq
(see section \ref{sect:DEfurther}).
%
For this workshop,
along with other projects,
I have written an R package,
\verb+rnaseqWrapper+,
that wraps these steps into
a single function,
with sensible defaults.
%
For those of you accessing these materials from outside of the workshop,
please contact me directly for the most updated version of the package.

\needspace{15em}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Install rnaseqWrapper} %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:DEseqInstallPackages}

To install this R package,
open RStudio (this will also work in R directly)
and follow these steps
(only necessary the first time you install it,
or if you update it).
%
Note that the additional dependencies are only required by a few of the functions,
but that includes the ones you will be using in these chapters.
%
This will likely take five to ten minutes.


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}

\hlcom{## Install MPP's rnaseqWrapper package}
\hlkwd{install.packages}\hlstd{(}\hlstr{'rnaseqWrapper'}\hlstd{)}

\hlcom{## Install DESeq and topGO from Bioconductor}
\hlkwd{source}\hlstd{(}\hlstr{"http://bioconductor.org/biocLite.R"}\hlstd{)}
\hlkwd{biocLite}\hlstd{(}\hlkwd{c}\hlstd{(}\hlstr{"topGO"}\hlstd{,}\hlstr{"Rgraphviz"}\hlstd{,}\hlstr{"DESeq"}\hlstd{))}

\hlcom{## To check if installed correctly}
\hlcom{## This is the only line you will need to run to load the package in the future}
\hlcom{## When this loads correctly, a message will list what is loading}
\hlkwd{library}\hlstd{(rnaseqWrapper)}
\end{alltt}
\end{kframe}
\end{knitrout}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\subsection{Running DESeq with rnaseqWrapper} %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{subSec:runDESeq}

As before, we need to start by loading in our expression data,
using the outputs from RSEM.
%
So, use \verb+setwd()+ to navigate to the directory
in which you saved the data files above.
%
You will likely want your working directory to be (at least)
one level above the directory where you saved the data,
largely to make it simpler to create analysis output directories,
but that is just personal preference of mine.
%
Once the data are downloaded,
the following function (part of my rnaseqWrapper package)
will automatically read in and merge the files together,
naming each column using the file names.
%
Read the help file for more information if you are interested
(\verb;?mergeCountFiles;).

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{## Load the data you just downloaded to your computer}
\hlcom{#  Note, you will likely have saved the outputs in a different directory}
\hlstd{setwd(}\hlstr{"path/to/data/"}\hlstd{)}
\hlstd{countData} \hlkwb{<-} \hlkwd{mergeCountFiles}\hlstd{(}\hlstr{"geneResult/"}\hlstd{)}

\hlcom{# View the data}
\hlkwd{head}\hlstd{(countData)}
\end{alltt}
\end{kframe}
\end{knitrout}


As you can see,
the files are now merged,
and we can work with them together.
%
Now, DESeq expects count data,
as its internal normalization,
and the method it uses to determine significance,
rely on raw counts.
%
The SEM column ``expected\_count'' is roughly raw counts,
with just a small amount of uncertainty given for the assignment of reads.
%
As such, we can use those columns for DESeq,
but we need to let DESeq know that they are counts
by rounding the data to integers.
%
To extract those columns,
we will use the function \verb;grep();
which searches for matches between its first argument
(e.g. ``expected\_count'' below)
in its second argument
(e.g. the names of the columns)
and returns the index (position)
of the matches.
%
I also like to remove the common portion of the column names,
using \verb+gsub()+,
which adds an argument to replace the matched portion of the text.
%
Both accept regular expressions,
which makes them very powerful,
but also a lot more confusing.
%
For now, just know that \verb;gsub(); is a nice way to systematically change things,
and that you can replace the empty quotes (``'')
with different text if you just want to change the names
(e.g. to ``\_reads'').

\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{## Extract just the read counts, and round them}
\hlstd{myCountData} \hlkwb{<-} \hlkwd{round}\hlstd{( countData[,}\hlkwd{grep}\hlstd{(}\hlstr{".expected_count"}\hlstd{,}\hlkwd{names}\hlstd{(countData))],}\hlnum{0}\hlstd{)}

\hlcom{## Trim the names to make the plots a bit nicer:}
\hlkwd{names}\hlstd{(myCountData)} \hlkwb{<-} \hlkwd{gsub}\hlstd{(}\hlstr{".expected_count"}\hlstd{,}\hlstr{""}\hlstd{,}\hlkwd{names}\hlstd{(myCountData))}

\hlkwd{head}\hlstd{(myCountData)}
\end{alltt}
\end{kframe}
\end{knitrout}


Finally, we can run DESeq on these rounded data.
%
For now, the basic defaults will work just fine,
but read the help documentation for more information
on how you might tune this for your own project.
%
For now, it is enough to know that the function takes
your count data,
searches for the ``conditions'' (e.g. sexes or treatments)
in the column names,
runs a differential expression test,
and saves the results
(both as an R object, and in your working directory,
preceded by the ``outNamePrefix'').
%
If your samples don't include your treatment names,
you can also specify ``conds'', which
lists each sample's treatment.


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{## Run DESeq}
\hlstd{deOut} \hlkwb{<-} \hlkwd{DESeqWrapper}\hlstd{(myCountData,} \hlcom{# Our count data to use}
                       \hlkwc{conditions}\hlstd{=}\hlkwd{c}\hlstd{(}\hlstr{"male"}\hlstd{,}\hlstr{"female"}\hlstd{),} \hlcom{# conditions to compare}
                       \hlkwc{outNamePrefix}\hlstd{=}\hlstr{"DESeqTest/"}\hlstd{)} \hlcom{# Where to save the outputs}
\end{alltt}
\end{kframe}
\end{knitrout}

This function goes through all of the basic steps of DESeq.
%
% with sensible defaults,
% many of which can be modified in the function.
%
Occasionally,
especially with very small or low-count data,
this function will throw an error stating that
the dispersion method failed.
%
This means that the default included did not work,
and may indicate problems with the underlying data.
%
In the future, the package will offer alternatives for this step,
and other steps,
but for now, you will have to work through each step separately,
in order to solve this problem.


The function saves the outputs in a few different, useful, formats:
\begin{itemize}
 \item A series of pdf plots that show some basic characteristics of the data
 \item Tab-delimited output of the differential expression test
 \item Tab-delimited output of the normalized read counts (if requested), and
 \item An R object which we can directly manipulate.
\end{itemize}

The first few items are all useful for record keeping
and visual inspection.
%
Open up the directory where you saved the data,
and look at the plots it generated.
%
%
The last item holds all of the generated data (but not plots) in a single object,
which allows us to explore our results in more depth.
%
So, we will save it now to make it easier to reload in the future
(instead of having to use \verb+read.table()+ for each element)
when you start analyzing our data.
% \todo{Add link to next chapter}
%
Use the below code to save the R object.


\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{## Save the DESeq R object:}
\hlkwd{save}\hlstd{(deOut,}\hlkwc{file}\hlstd{=}\hlstr{"DESeqTest/deOut.Rdata"}\hlstd{)}
\end{alltt}
\end{kframe}
\end{knitrout}

Before exiting R,
spend a few minutes playing with the data you have just generated.
%
Make some plots,
see what is available,
and just generally fiddle.
%
As you read the next section,
and we discuss it,
play with the data you have,
and think about what you might do with it
to meet the objectives of \emph{your} study.


One place to consider starting is by generating a heat map,
one of the standards in gene expression analysis.
%
R makes this very easy to do,
and the \verb;rnaseqWrapper; package includes
a wrapper to make it (somewhat easier).
%
A similar plot is automatically generated 
by \verb;DESeqWrapper(); and saved in the pdf output,
but this will give you the power to tweak the plot to
your purposes.




\begin{knitrout}
\definecolor{shadecolor}{rgb}{0.969, 0.969, 0.969}\color{fgcolor}\begin{kframe}
\begin{alltt}
\hlcom{## Save oval for easier use}
\hlstd{myQvals} \hlkwb{<-} \hlstd{deOut}\hlopt{\$}\hlstd{deOutputs}\hlopt{\$}\hlstd{malevsfemale}\hlopt{\$}\hlstd{padj}

\hlcom{## Limit data to interesting genes}
\hlstd{toPlot} \hlkwb{<-} \hlkwd{as.matrix}\hlstd{(myCountData[myQvals} \hlopt{<} \hlnum{0.05} \hlopt{& !}\hlkwd{is.na}\hlstd{(myQvals),])}

\hlcom{## Make a heatmap}
\hlkwd{heatmap.mark}\hlstd{(toPlot}\hlstd{)}

\hlcom{## Add color and a legend to show more of the options}
\hlkwd{heatmap.mark}\hlstd{(toPlot,}
             \hlkwc{cexCol}\hlstd{=}\hlnum{.9}\hlstd{,} \hlcom{# make column labels smaller}
             \hlkwc{ColSideColors} \hlstd{=} \hlkwd{rep}\hlstd{(}\hlkwd{c}\hlstd{(}\hlstr{"red"}\hlstd{,}\hlstr{"blue"}\hlstd{),}\hlkwc{each}\hlstd{=}\hlnum{8}\hlstd{),} \hlcom{# match column order}
             \hlkwc{scaleLabel}\hlstd{=}\hlstr{""}\hlstd{)} \hlcom{# turn off label to leave room}

\hlkwd{legend}\hlstd{(}\hlkwc{x}\hlstd{=}\hlstr{"topleft"}\hlstd{,}\hlkwc{inset}\hlstd{=}\hlkwd{c}\hlstd{(}\hlopt{-}\hlnum{.01}\hlstd{,}\hlnum{.13}\hlstd{),} \hlcom{# where the label should go}
       \hlkwc{bty}\hlstd{=}\hlstr{"n"}\hlstd{,} \hlkwc{cex}\hlstd{=}\hlnum{.5}\hlstd{,} \hlcom{# no box around it, and set the size}
       \hlkwc{legend}\hlstd{=}\hlkwd{c}\hlstd{(}\hlstr{"female"}\hlstd{,}\hlstr{"male"}\hlstd{),} \hlcom{# What should be there}
       \hlkwc{fill}\hlstd{=}\hlkwd{c}\hlstd{(}\hlstr{"red"}\hlstd{,}\hlstr{"blue"}\hlstd{),} \hlcom{# Colors to use}
       \hlkwc{title}\hlstd{=}\hlstr{"Conditions"}\hlstd{)} \hlcom{#Label for the legend}
\end{alltt}
\end{kframe}
\end{knitrout}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Interpreting differential expression analysis} %%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

This section provides a thin background on differential expression analysis
that will be used to guide our in class discussion.
%
Hopefully, the details in this section will help to
guide your decision making for your own data interpretation.


There are a plethora of gene expression analysis tools,
and there seem to be more emerging every week.
%
Some of these are just small tweaks to increase speed or user\-/friendliness,
but some make major changes to the underlying assumptions and math.
%
In particular,
different differential expression tools rely on different mathematical models
(e.g. normal vs. negative binomial distributions)
to estimate the probability that identified differences are real.
%
The choice of which program is best for you and your data is unique,
and must address ease of use,
mathematical assumptions,
the details of your project,
and what you are attempting to achieve.
%
For most analyses,
the results will be relatively similar,
and there are few \emph{bad} choices.


All of these programs,
and likely all of the ones still to come,
will give outputs in a similar fashion.
%
A table with each analyzed gene as a row.
%
The columns will represent the basic statistics and may include:
average expression level (for each group or for the two combined),
difference between the two (fold-difference, or log-fold-difference),
a statistical test,
and the FDR level of significance.
%
No matter what your specific project,
focusing on the significant genes
(those below an a priori FDR cutoff)
and the direction of their difference is likely to be most fruitful.
%
Many programs will write just these genes to a separate file to make this analysis easier.


From here,
there are many options to progress.
%
One is Gene Ontology (GO) analysis.
%
If your species annotation includes GO terms,
you can determine which GO terms are over\-/represented.
%
As above,
there are many programs for doing just this
and they all work on similar principles.
%
The idea is to find GO terms that are more common
among significant genes than among the full set of annotated genes.
%
That is, a GO term that is present 5 times among significant genes
(that is, 5 of the significant genes are annotated with that GO term)
is more likely to be meaningful if
there are only 5 genes with that annotation in the full set,
than if there are 50 genes with that annotation.
%
GO analysis provides a nice snapshot of what is
being regulated differently between your two groups.


Alternatively,
you can focus on individual genes that are differentially expressed.
%
This is particularly fruitful if you have an a priori
reason to expect a gene to vary.
%
For example,
if your sample groups are a model for a disease or developmental state,
you may expect known marker-genes to differ between the groups.
%
Identifying such differences can help to confirm
the validity of your experimental set up.
%
In addition,
you can search through the gene lists for genes that
may play interesting and related functions.
%
Identifying these genes may help to guide
future research projects or
to connect your findings to those of other research systems.


These approaches are neither exhaustive,
nor mutually exclusive.
%
The simplest answer to ``what should I do now?''
is to play with your data and see what emerges.
%
You may be surprised to find a set of key genes
from another system
(another disease, for example)
differentially expressed between your groups.
%
Breaking down the gene lists
(e.g., by GO term, by pathway, or even randomly)
will allow students a great deal of autonomy
and may lead to some surprising and insightful conclusions
as they dig deeply into a subset of your analysis.




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Differential expression analysis,
despite being studied for a long time now
(qPCR, microarrays, Northern blots, etc.),
is still an unsettled field.
%
Especially as datasets grow
(now analyzing tens of thousands of genes simultaneously),
there is great debate over many basic questions.
%
How can we best control for known and unknown sources of error?
%
How should multiple-testing be corrected?
%
How can we ensure we don't reject too many true differences?
%
Does fold-change matter?


The downside is that there are many open questions
and there are rarely simple answers.
%
You may need to analyze your data in several different fashions
in order to figure out what works best for your system, questions, and needs.
%
The upside is that there is not one ``right'' way to do things,
which allows both you and your students substantial leeway
to explore your data in novel ways.
%
Who knows, your approach may even revolutionize the field.


All this is to say:
the answers for how to best analyze your data probably lie in your data,
not with someone else.
%
Answers to simple questions abound on places like SeqAnswers,
and in the documentation to your programs of choice.
%
However, you may need to try several different approaches
before the answers you get seem
to completely address the questions you started with.
%
List-serves and outside guidance are great
to make sure that we are not fooling ourselves,
and should be utilized whenever possible.
%
Just, don't expect a clean answer.


As a final note,
it is easy with large datasets to fool yourself.
%
It is important to get critical external feedback to your methods.
%
Simulating your data analysis can also be a great way to make sure
that your results are not just a product of some unseen bias.
%
I came three days away from submitting
(in a revision no less)
an analysis that was, quite frankly, completely wrong.
%
I had simulated at a downstream step,
and missed an underlying artifact in my data.
%
The conclusion,
around which I had based most of that paper and several talks,
was just flat wrong.
%
It was an artifact and did not represent anything meaningful.
%
Be careful in your analyses,
and always listen when others raise concerns about your approach.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:DEfurther}

More information on the \verb;DESeq; package,
including the full package vignette
and a link to the full paper
(available open access at the link below)
can be found at:
\begin{itemize}
%  \item \url{http://cufflinks.cbcb.umd.edu/ }
%  \item \url{http://cufflinks.cbcb.umd.edu/manual.html#cuffmerge}
 \item \url{http://www.bioconductor.org/packages/release/bioc/html/DESeq.html}
 \item \url{http://genomebiology.com/2010/11/10/R106/}

\end{itemize}


% \begin{knitrout}\begin{kframe}\begin{alltt}
% code here
% \end{alltt}\end{kframe}\end{knitrout}
