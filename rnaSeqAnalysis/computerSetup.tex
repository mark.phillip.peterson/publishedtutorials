%% Chapeter on Computer setup

\chapter{Computer setup}
\label{chap:computerSetup}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

RNA-seq analysis requires a lot of computer resources
to handle the large datasets that are generated.
%
Even a simple experiment can quickly run up near terabytes of data,
and handling this data requires specially designed programs.
%
Most of these programs can be run on your own computer,
but they often take a very long time
and slow your computer down to the point of no longer being usable.
% Merge these two pp's
One simple solution to this problem is to run your analyses on a different computer,
such as
a Linux-based computer-cluster/supercomputer.
%or a workflow system (such as Galaxy) with a graphical user interface (GUI).
%
In this module, we will walk through the process of using Linux for sequence analysis.
%
In a later chapter,
we will introduce a few alternatives,
including web based platforms
(See chapter \ref{chap:easyButton}).



One important thing to keep in mind:
these tools are constantly changing
as next-generation sequencing is still an emerging field.
%
The instructions here will include details for the specific platforms
and programs that will be used here,
but will also explain what is happening,
so that you can generalize to other applications.
%
In general,
remember that ``Google is your friend'' (GIYF) \textemdash
someone else has almost always encountered problems similar to yours,
and answers to their questions are usually available online
with a little Google-fu.
%
Second, most of these programs and computer resources come with
extensive documentation that can walk you through
many of the basic steps.
%
When in doubt, ``read the manual'' (RTFM)
and many of your questions may be addressed.
%
All users, even advanced users,
often do not remember the correct commands,
but are just better at searching for the answer:

\begin{figure}[h]
\begin{center}
\includegraphics[width=.9\textwidth]{../images/xkcd_tar_1168}
\caption{\href{http://xkcd.com/1168}{xkcd.com/1168}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Learn how to access a computer cluster or supercomputer
%  \item Learn how to access Galaxy and similar workflow systems
 \item Learn how to install necessary software
 \item Learn how to transfer data to and from these systems
 \item Understand a basic Linux command line interface
 \item Troubleshoot simple computer system problems
%  \item Run simple commands on both types of systems
\end{itemize}


% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to communicate and collaborate with other disciplines
%  \item Ability to use modeling and simulation
% \end{itemize}


%% Galaxy stuff?
%% Get a computer account
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Sign on to a Linux computer system} %%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Signing into a computer cluster or supercomputer is a bit more complicated than
signing into your own computer.
%
However, once mastered,
it is straight forward to accomplish.
%
You first need a system capable of ``SSH'' (Secure Shell) connecting.
%
On Windows, you would need to install PuTTY to be able to gain access
(search for ``putty ssh''  and follow the directions to install
selecting ``Windows installer for everything except PuTTYtel'').
%
In Unix or Mac, the terminal comes pre-installed with this ability,
just open a terminal window
(\verb;Ctrl+Alt+t; in Ubuntu,
Applications $\rightarrow$ Utilities $\rightarrow$ ``Terminal'' in Mac).
%
The basic command,
which can be adapted to fill in the PuTTY screen
is then:

\begin{knitrout}\begin{kframe}\begin{alltt}
ssh \hlnum{user}@\hlnum{hostname}
\end{alltt}\end{kframe}\end{knitrout}

where ``user'' is your assigned user name on the system,
and ``hostname'' is the address you are attempting to connect with:
either an address or an IP address.
%
Thus, for our system it will look like:


\begin{knitrout}\begin{kframe}\begin{alltt}
ssh \hlnum{user}@\computerAddress{}
\end{alltt}\end{kframe}\end{knitrout}

You will then be asked for your password.
%
Enter it here (the cursor will not move as you type),
and you should be granted access to the system.
%
The first time you log on to the super computer,
it will likely ask you to select your shell.
%
There is substantial debate about the ``best'' shell,
but they will all function similarly for your needs.
%
All of the examples in this workshop will use the BASH shell,
so please select that one.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Linux tutorial} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

You will now have a command line prompt,
and will be able to navigate as you would on any Linux system.
%
We will go through detailed commands as we go through these modules,
however, to get a feel for the Linux system,
please work through this (wonderful) online tutorial:\\
\url{http://www.ee.surrey.ac.uk/Teaching/Unix/}.
%
A few notes on this tutorial:
\begin{itemize}
 \item {Some of the commands are specific to that University's file system,
	but they offer clear ways to work around that.
	For example, you may need to use the following to download one of the files:}
	  \begin{knitrout}\begin{kframe}\begin{alltt}
wget http://www.ee.surrey.ac.uk/Teaching/Unix/science.txt
	  \end{alltt}\end{kframe}\end{knitrout}
	
 \item {They are using a a different shell (tcsh instead of bash),
	which leads to a few small differences
	(e.g., the prompt in bash is '>' instead of '\%'
	and in 8.4, the files they are referring to are
	called '.profile' and '.bashrc' instead of
	login and cshrc. Just a thing to be aware of.}
 \item {They suggest using 'nedit' to modify text files.
	But, it doesn't come standard on Linux.
	For this tutorial, replace `nedit' with `nano',
	which runs from the command line.
	Use \verb;Ctrl+o; to save (for ``out''),
	and \verb;Ctrl+x; to exit.}
\end{itemize}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Loading data} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

\label{sect:SetupLoad}

There are three basic ways we are going to get data on
to computer for this class:
scp, wget, and copying from things already loaded for you.
%
First, let's try loading something from your local machine using \verb;scp; (Secure Copy).
%
Open a second terminal window on your computer
(Windows users, see the below for more detailed directions).
%
You will then need to move (using ``\verb;cd folderName;'')
to a folder that contains the data you wish to upload,
then enter the \verb;scp; command.
For example:

\begin{knitrout}\begin{kframe}\begin{alltt}
cd \hlnum{folderName}
scp \hlnum{myFile.txt} \hlnum{user}@\computerAddress{}:
\end{alltt}\end{kframe}\end{knitrout}

In Windows,
the command structure is a bit different.
%
Access a terminal window by hitting \verb;Ctrl+ESC;,
typing ``R'', typing ``cmd'' and then hitting \verb;Enter;.
%
From here, \verb;cd; to the appropriate folder,
as above,
then enter the command.

\begin{knitrout}\begin{kframe}\begin{alltt}
"C:\textbackslash{}Program Files\textbackslash{}PuTTY\textbackslash{}pscp.exe" \hlnum{myFile.txt} \hlnum{user}@\computerAddress{}:
\end{alltt}\end{kframe}\end{knitrout}

In all places where you are told to use \verb;scp;,
you will have to use this full path instead.
%
The path to the ``pscp.exe''
file must match the location of your installation
(the folder you installed PuTTY into),
so double-check the path if you get an error
(and make sure you are using backslashes here,
but only in Windows as they have not adopted the standard convention).
%
You can also add that path to your ``path'' Environmental Variable
to avoid having to retype it every time,
but that is outside the scope of this module.


Note that quotation marks are necessary if there is a space in the directory name
for any of the paths you are using
(e.g. ``Program Files'' or ``GCAT Workshop''),
which is one (of many) reasons to avoid spaces in file and directory names.
%
% The tilde (\textasciitilde) in the command is where you can type
% the directory that you want the file to go to,
% for example ``\textasciitilde/sequences''
% with the tilde representing your home directory on the remote system.


Alternatively, you can use \verb;wget; to download a file.
%
The format is similar to \verb;cp; and \verb;scp;,
just with a url in place of the source file
and that the source file name is kept
(but stored in your current directory).
%
This command works with any download-able file,
as you saw when working through the tutorial.
%
We will use it most often for downloading programs to install,
but it is also a nice way to download some data.


Finally, all of you have access to a shared directory,
which we will be using extensively,
named `\dataDirectory{}'.
%
Copy the file 'testFile.txt' into your own home directory,
using \verb;cd; and \verb;cp; as necessary.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
The great thing about Linux is that there is a large community,
so every problem you encounter has likely been faced (and solved)
by other users who are happy to share their solutions.
%
In addition,
nearly every package comes with (at least)
one of two simple ways to pull up more information:

\begin{knitrout}\begin{kframe}\begin{alltt}
man \hlnum{packageName}
\hlnum{packageName} --help
\end{alltt}\end{kframe}\end{knitrout}

These two commands will pull up a lot of information about
the syntax and options available for that software,
often with usage examples.
%
At the very least,
it may identify the option you need help with:
a great way to target your search for more information.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
More information related to these topics can be found in:
\begin{itemize}
 \item Any Unix/Linux guidebook, including great online resources
 \item The man and help pages of the package(s) you are interested in
\end{itemize}
