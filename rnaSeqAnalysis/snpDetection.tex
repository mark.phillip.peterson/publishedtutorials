%% Chapter on QC and processing
\chapter{Variant Detection}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Everything before this chapter could have been done
using microarrays.
%
There is debate in the literature about when RNA-seq and microarrays are more accurate;
however, there is one area in which there is a clear winner.
%
RNA-seq analysis adds the ability to detect genetic differences.
%
This added benefit provides a whole new world to explore.
%
% This workshop is too short,
% and the work to this point too long,
% to dig deep into genetic variant testing. 


This chapter will work through the very basic levels of genetic analysis of RNAseq data.
%
Where you take it from there is up to you.
% %
With the small sample sizes of most of the groups here,
population genomic tests may be out of reach.
%
It may, however, be possible to focus on genes of interest
(e.g., those in known pathways or those identified as differentially expressed)
to add an intriguing layer to your classroom analysis.
%
In addition, with model systems,
the role of many of these genetic variants may be known already.
% %%%%%%%%%
%
For now, we will simply focus on the tools that are available,
and the analyses that are possible.
%
There are many different options for the tools to use,
but many rely on the same bowtie outputs generated for expression analysis.
%
Today we will use just one of the downstream options.


This chapter is likely to contain less detail than others on the process,
and I strongly encourage you to read the documentation included in ``Further Reading''
before applying these approaches to your project.
%
Like differential expression,
genetic variant detection is suffering from a crisis of statistics.
%
The datasets are far larger than can be handled by
many of the early variant detectors
(or population genetics methods),
and new methods are lagging behind the increases in sequence production.
%
The biggest problem is differentiating between sequencing/alignment errors and true variants.


% These concerns,
% combined with the need for interpretation to be species and experiment specific,
% mean that there is little general information that I can provide to you.
% %
% With datasets this size,
% we may simply need to play with the results and see what comes out.



\begin{figure}[h]
\begin{center}
\includegraphics[width=.7\textwidth]{../images/xkcd830_genetic_analysis}
\caption{\href{http://xkcd.com/1168}{xkcd.com/1168}
	 (Creative Commons Licencse)
}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
%  \item Discuss the concepts of SNP detection
%  \item Discuss statistical questions surrounding genetic variants
 \item Learn the basics of SNP detection and ideas for where to go next
 \item Understand the basic principles managing large datasets
 \item Understand the statistical principals of large datasets
\end{itemize}

% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \section{Vision and Change Competencies addressed}  %%%%%%
% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% \begin{itemize}
%  \item Ability to apply the process of science
%  \item Ability to use quantitative reasoning
%  \item Ability to use modeling and simulation
% \end{itemize}
% 


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Set up the software} %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

We are going to be using a tool named VarScan in this chapter,
which requires the samtools package as support.
%
Samtools is available as a module on many computer systems,
and VarScan is a simple java file
(like Trimmomatic).
%
Recall that we have previously installed and loaded java,
but if you missed that step,
you will need java to run the below
(see section \ref{sect:QCsetUpFQ}).
%
To install these tools,
use the following commands,
which first download VarScan,
then load samtoools,
then set samtools to load automatically.


\begin{knitrout}\begin{kframe}\begin{alltt}
cd \mytilde/opt
wget http://downloads.sourceforge.net/project/varscan/VarScan.v2.3.9.jar

module load samtools
echo 'module load samtools' >> \mytilde/.modules
\end{alltt}\end{kframe}\end{knitrout}




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Prepare the alignment file} %%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For this example,
we will create an alignment file from a small subset of our
read files.
%
To run this for a more complete analysis,
it would be necessary to run both this step
(preparing the mpileup file)
and the next step (analyzing the output)
in a script submitted via \verb;qsub;.


Move into the directory with the sample sequence files,
then each of you will analyze a couple of the samples
(the sample you were assigned previously,
plus one other of your choice).
%
Note that we are using the same reference as what we used for REM,
 but we need to call the FASTA file directly.
%
Below, \hlnum{file} is the output from the \verb;rsem-calculate-expression;
run in section \ref{sect:MappingRun},
and \hlnum{file2} is any additional file
(i.e., one created by another student)
from the directory.


\begin{knitrout}\begin{kframe}\begin{alltt}
cd \dataDirectory{}/seqData
samtools mpileup -f transcriptome/gcatRef.transcripts.fa   \textbackslash
                 align/\hlnum{file}.transcript.sorted.bam  \textbackslash
                 align/\hlnum{file2}.transcript.sorted.bam \textbackslash
                 > tempData_\hlnum{yourName}.mpileup
\end{alltt}\end{kframe}\end{knitrout}

This creates an encoded file containing a lot of information about
the aligned sequence file.
%
Use \verb;less; to take a look at it.
%
As you can see, we need something to help us interpret it.
%
That is where VarScan comes in.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Run VarScan} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
VarScan is written as a java file,
which means we call it by calling java for the command of interest
and passing the options we want.
%
Below, we call the mpileup file we just created,
pass it a threshold p-value of interest,
then use the redirect (`>') to save the output.
%
We are accepting all of the defaults from  \verb;mpileup2snp;, 
but we could set several options to control the exact output.
%
Look at the documentation linked in ``Further Reading''
(section \ref{sect:SNPfurther})
for more details
and to understand the output.


\begin{knitrout}\begin{kframe}\begin{alltt}
java -jar \mytilde/opt/VarScan.v2.3.9.jar mpileup2snp \textbackslash
          tempData_\hlnum{yourName}.mpileup \textbackslash
          -p-value 0.01 > tempVarScan_\hlnum{yourName}.txt
\end{alltt}\end{kframe}\end{knitrout}

Use \verb;less; to look at the file.
%
Remember here that we only have a small number of reads
and samples, so the data are likely to be less than complete.
%
To explore these data more fully,
download the output file (using \verb;scp;)
and load it into R using \verb;read.table();.
%
From here,
there are several functions in the \verb;rnaseqWrapper; package
that might help you explore the data,
though many of these assume that the reference contains only 
an in-frame coding sequence (e.g., predicted cds),
which we don't have for these data
(the fasta reference is not necessarily in-frame,
and frequently includes un-translated regions).
%
However, for your projects, these may be useful:

\begin{description}
 \item [parseVarScan] {Separates the columns of varScan output format,
			which improves some usability.
			This step is not needed for downstream use,
			but may still prove helpful for other applications.}
 \item [kaksFromVariants] {Calculate Ka/Ks ratios using the identified variants}
 \item [nSynNonSites] {Calculate the number of synonymous and non-synonymous sites
			in genes from a reference.
			Useful to complement the Ka/Ks output for genes with no identified variants.}
 \item [determineSynonymous] {Determines whether each variant is non-synonymous or synonymous
				compared to the reference position.
				Also calculates dN/dS (wihout respect to sites).}
 \item [calculateThirdPosBias] {Calculate the portion of variants (in each gene)
				  at each codon position.
				  A nominal proxy for Ka/Ks and dN/dS, if needed.
				  When no reference is used,
				  it assumes the most common variant position is the third position.}
\end{description}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Run a more complete analysis} %%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
Now, to put all of these together, \emph{I} ran the commands below
to create a single file to analyze.
%
Note that  the `` * '' indicates any match,
so, it will match all of the temp sequence files in the directory.
%
This is just one more reason why you should be extra careful
to be consistent in file naming conventions.

\begin{knitrout}\begin{kframe}\begin{alltt}
\colorbox{black}{\color{white}\textbf {Do NOT run this:}}

samtools mpileup -f transcriptome/gcatRef.transcripts.fa \textbackslash
                 align/*.transcript.sorted.bam > tempData.mpileup

java -jar \mytilde/opt/VarScan.v2.3.9.jar mpileup2snp \textbackslash
           tempData.mpileup -p-value 0.01 > tempVarScan_defaultOptions.txt
\end{alltt}\end{kframe}\end{knitrout}

This created a file with all of the samples analyzed,
and may be more interesting to analyze.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
My background is (definitely) not in population genetics,
and my research has focused largely on gene expression analysis.
%
However, I have gotten good at reading programmatic documentation,
and in time you will too.
%
So, when you start your project,
start with the manuals and documentation,
and go from there.
%
In addition, there are a lot of really helpful examples online,
including answers to other people that faced issues similar to what
we are likely to encounter.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\label{sect:SNPfurther}
More information related to these topics can be found in:
\begin{itemize}
 \item \url{http://varscan.sourceforge.net/using-varscan.html}
 \item \url{http://en.wikipedia.org/wiki/Nucleic\_acid\_notation#IUPAC\_notation}
\end{itemize}
