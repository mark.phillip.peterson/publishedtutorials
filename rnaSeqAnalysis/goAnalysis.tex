%% Chapter on QC and processing
\addtocontents{toc}{\newpage}
\chapter{Functional group analysis}
\label{chap:GOanalysis}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Background} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


After all the hard work of generating RNA-Seq data,
one is rewarded with a (hopefully) long list of genes
differentially expressed
between treatments, or mutants, or tissues, or 
whatever you were testing for.
%
\ldots
Now what?

Human nature expects highly meaningful genes to leap to the top of these hard-earned lists,
but in reality there are usually hundreds to thousands of differentially expressed genes
and too much information to easily comprehend or summarize.
%
How can we extract meaningful information from such large lists?


One approach is to consider 
what \emph{kind} of genes are being affected.
%
From a gene list, especially a long one,
it is difficult to see a larger pattern of gene types being affected,
in part because it would require knowing what every gene does.
%
This is where Gene Ontology (GO) annotation comes in.
%
The GO system is a heirarchical set of gene function annotations,
which has become the de facto standard across all species.
%
This works because the functions are defined in a way that applies
from plants to animals to bacteria.


This annotation is assembled into a ``graph'' to show the ways
that functions are related.
%
% This also allows genes to be assigned to more specific functions
% as more is learned about them.
%
For example, a hormone receptor might be annotated with the function
``response to hormone'' and when more is learned, further assigned to
the more specific function ``response to steroid hormone.''
%
Further, genes can be assigned to any number of functions,
reflecting the wide range of roles that any given gene might play.
%
Our hormone receptor, for example,
might also be annotated as ``sequence-specific DNA binding''
if it plays a role in regulating gene expression.


By utilizing this type of annotation,
we can move beyond a list of (often cryptic) gene names.
%
A list of affected functions is much smaller,
and often much more intuitive to interpret.
%
This can make presenting a coherent story around your data much simpler,
and is a great guide for both the investigator and readers.
%
In this chapter, we will cover the basics of these analyses,
and how to conduct them.


\begin{figure}[h]
\begin{center}
\includegraphics[width=.45\textwidth]{../images/dataInfoAsCake_JohnsonEpicGrapic_bw}
\caption{\href{http://epicgraphic.com/data-cake/}{epicgraphic.com/data-cake/}}
\end{center}
\end{figure}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Chapter goals} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\begin{itemize}
 \item Understand the basics of GO annotation
 \item Understand the statistics of GO analysis
 \item Be able to run a GO analysis
 \item Be able to interpret the output of a GO analysis
\end{itemize}


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Basic statistics} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Functional group analysis aims to
determine which GO annotations are enriched
(over-represented) 
in subsamples of gene lists
(e.g., differentially expressed genes;
referred to as ``DE'' below).
%
Functional annotations that appear disproportionately among
DE genes
are likely to play a role in the phenotypic response to your test;
those that appear with equal probability in
DE genes and all genes are not.


%
In Table \ref{table:GO},
we see that we are trying to show that there are
more genes in the ``A'' position,
than expected by chance.
%
Put simply:
we want to know if the ratio
$\frac{A}{B}$
is significantly greater than the ratio 
$\frac{C}{D}$.
%
In this way, a GO function assigned to 11 genes in the dataset,
10 of which are significantly DE,
is much more meaningful (and over-represented),
than a function assigned to 300 genes,
10 of which are significantly DE.



\begin{table}[h]
\begin{center}
\begin{tabular}{ |l | c c |}
      \hline
   & is Sig DE & not Sig DE\\ \hline
  Has GO term & \textbf{\hlstr{A}} & B \\
  Not this GO & C & D\\ \hline
\end{tabular}
\caption{Depiction of functional group testing}
\label{table:GO}
\end{center}
\end{table}

To put this more formally,
functional analysis asks whether the distribution
of GO annotation and significance are indepenent.
%
These analyses generally use familiar statistical principles
for contingency tables,
such as Chi-square and Fisher's exact tests.
%
The p-values from these tests reflect the probability of enrichment of
particular functional groups among DE genes.
%
However, because many tests are being performed,
we must adjust for multiple testing,
generally using false discovery rate correction
(similar to what we used in Chapter \ref{chap:DE}).
%
% Hence, functional group analysis tools typically show
% not only a P-value but an adjusted P-value
% that accounts for multiple testing.

One important point to consider is what set of genes should be
included in the background (full) set.
%
It is tempting (and often a default for many programs)
to use all of the genes in the genome as reference set.
%
However,
experiment-specific detected genes are almost always
more appropriate background lists.
%
Because one tissue may not express all genes,
the use of a genome-wide background will then also reflect
tissue specificity,
which is presumably of less interest.
%
In addition, if many genes from a functional group are not expressed,
they cannot be DE, which
means that, even all of the expressed genes in the group are DE,
the GO category may still not be identified as over-represented.



Finally, be aware that the statistical power of functional group analysis
is highly dependent on the number of genes in the selected list.
%
Lists of 1 to 10 genes have little power to detect enrichment
of functional groups whereas 300 to 3,000 DE genes
are ideal in experiments with a background list of 5,000 to 20,000 genes.

\needspace{5em}
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Getting GO annotations} %%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Unfortunately,
despite the standardization of the GO system,
the identification of GO terms for your organism
can still be tricky.
%
Each study system is different,
each has their own repositories,
and those may (or may not)
be directly linked to the gene names in your particular reference.
%
So, head to trusty google, and start looking for resources
for your organism.
%
If you hit problems, seek out assistance from colleagues,
or list serves (including the GCAT-SEEK listserve).
%
Others have probably faced (and overcome) the same hurdles you face,
and they are often happy to help.


This problem is worse in species with limited genomic resources, as
non-model species have little or no GO functional annotation.
%
This lack can be overcome by determining the best hit
(generally at the protein level using blastp)
for each gene against a (hopefully closely) related model species
(such as Drosophila, Arabidopsis, Mus, etc.).
%
Tools, such as Blast2GO,
can greatly simplify this process,
but know that it is likely to take a long time.



Results carry the assumption that blastp homology confers
meaningful information about function across distantly related species.
%
Conservation of gene function is sufficiently strong
that this is a fairly safe assumption.
%
However, inferences about the function of
specific genes remains putative and should be treated as such.
%
The key is to recognize both the possibilities and the limitations,
and neither discard the bad because it isn't perfect,
nor over-interpret the good as if it were perfect.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Run a GO analysis} %%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%


Finally, we are ready to run a GO analysis.
%
There are a \emph{lot} of tools available to run these analyses,
including graphical programs (such as BiNGO in Cytoscape)
and web based tools (such as David, described in Section \ref{sect:David}).
%
A full, and very long, list of such tools is available at:
\url{http://neurolex.org/wiki/Category:Resource:Gene_Ontology_Tools},
if you are interested.
%
However, today, we will be working with the R package \verb;topGO; from BioConductor.


In RStudio on your computer,
load the \verb;topGO; and \verb;rnaseqWrapper; packages,
which you installed in Section \ref{sect:DEseqInstallPackages}
(remember that a package only needs to be installed once on each computer).


\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Run GO analysis}
\hlcom{## Mark Peterson 2014 June 06}

\hlcom{## Set working directory}
\hlkwd{setwd}\hlstd{(}\hlstr{"path/to/data/"}\hlstd{)}

\hlcom{## Load topGO package and rnaseqWrapper}
\hlkwd{library}\hlstd{(topGO)}
\hlkwd{library}\hlstd{(rnaseqWrapper)}
\end{alltt}\end{kframe}\end{knitrout}



Then we will load our annotations,
which you downloaded with the RSEM results.
%
This includes a list of GO annotations developed for the dark-eyed junco,
in a format usable by \verb;topGO;.
%
Your GO annotations may come in a different format,
so take note of the structure in the tsv file to guide you.
%

\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Load the GO annotation file}
\hlstd{goAnno} \hlkwb{<-} \hlkwd{readMappings}\hlstd{(}\hlstr{"annotations/goAnnotation.tsv"}\hlstd{)}
\end{alltt}\end{kframe}\end{knitrout}

\needspace{7em}
We also need to load in the \verb;DESeq; results
(saved in Subsection \ref{subSec:runDESeq}).

\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Read in DESeq output}
\hlcom{## This loads in an R data object, and returns the name of the object(s)}
\hlcom{## So, "deOutName" has the name of the data we actually want to use}
\hlcom{## Here, that is our DESeq object "deOut"}
\hlstd{deOutName} \hlkwb{<-} \hlkwd{load}\hlstd{(}\hlstr{"DESeqTest/deOut.Rdata"}\hlstd{)}

\hlcom{## Set an easier name to access the part you are interested in:}
\hlstd{myDE} \hlkwb{<-} \hlstd{deOut}\hlopt{\$}\hlstd{deOutputs}\hlopt{\$}\hlstd{malevsfemale}
\end{alltt}\end{kframe}\end{knitrout}


\needspace{5em}
Now, we need to identify 
our ``expressed'' background.

\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Which Genes were 'expressed' (using mean expression over 5 counts)}
\hlstd{expGenes} \hlkwb{<-} \hlstd{myDE}\hlopt{$}\hlstd{id[ myDE}\hlopt{$}\hlstd{baseMean} \hlopt{>} \hlnum{5} \hlstd{]}
\end{alltt}\end{kframe}\end{knitrout}

\ldots and our significantly DE genes. 
%
Note the use of ``\verb;!is.na(myDE$pval);''
This tells R to ignore unassigned p-values
(such as for genes with no counts),
otherwise R doesn't know what to do with NA values,
and returns a lot of ``\verb;NA;'' to warn us of that.

\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Which genes are significantly DE?}
\hlcom{## Normally, you would use the adjusted pvalue (but we have low power)}
\hlstd{sigGenes} \hlkwb{<-} \hlstd{myDE}\hlopt{\$}\hlstd{id[ myDE}\hlopt{\$}\hlstd{pval} \hlopt{<} \hlnum{0.05} \hlopt{& !}\hlkwd{is.na}\hlstd{(myDE}\hlopt{\$}\hlstd{pval) ]}
\end{alltt}\end{kframe}\end{knitrout}

Now, we can run a basic GO analysis,
using a wrapper from the \verb;rnaseqWrapper; package.
%
The function \verb;runGOAnalysis(); takes our lists of genes,
and GO annotations,
and runs all of the tests for us.
%
It defaults to running the ``Biological Processes'' ontology,
but can also be set to analyze ``Molecular Function'' or
``Cellular Component.''
%
Check out the help for more details on available options.
%
Here, we are going to run the analysis with the ``classic'' algorithm,
which works just as we described above: 
running a Fisher's exact test on every GO term we are analyzing.

\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Run GO analysis}
\hlstd{goOutput} \hlkwb{<-} \hlkwd{runGOAnalysis}\hlstd{(sigGenes,expGenes,goAnno,}
                          \hlkwc{pValThresh} \hlstd{=} \hlnum{0.05}\hlstd{,} \hlkwc{plotGO}\hlstd{ = }\hlnum{TRUE}\hlstd{,}
                          \hlkwc{algorithm}\hlstd{ = }\hlstr{"classic"}\hlstd{)}
\hlkwd{head}\hlstd{(goOutput)}
\end{alltt}\end{kframe}\end{knitrout}


Note that in the returned plot, darker colors are more significant.
%
See also that an entire series of GO terms is often calculated as
over-represented.
%
If only the very bottom node is truly over-represented, but
by a large degree,
it can cause the higher nodes
(which are also assigned to the genes of the lower node)
to appear over-represented as well.


Imagine, for example, that all 10 genes annotated to a low-level GO term
(say ``negative regulation of DNA ligation'')
are DE.
%
This GO term is clearly over-represented.
%
But, imagine that in the next level up 
(``regulation of DNA ligation''),
there are 5 additional genes 
(perhaps ``positive'' regulators),
and none of them are DE.
%
In most data sets, 10 out of 15 genes will still be 
scored as significantly over-represented,
even though it is only the ``negative'' regulators that we are interested in.



This non-indepenence makes it difficult to know
how to interpret each of the significant terms,
and leads to large philosophical questions on the
appropriate multiple-testing correction.
%
However, the \verb;topGO; authors have come up with a solution to this problem.
%
Essentially, they test to see which node in a chain is the \emph{most}
over-represented, and adjust their p-values to reflect that.
%
The math is quite complex, and beyond the scope of this tutorial;
however, the paper describing the approach is listed in Further Reading below.
%
In the end, it gives us a smaller list of GO terms that
is more focused on the real source of differnces.
%
We can see it in action by comparing our results from each method directly:


\begin{knitrout}\begin{kframe}\begin{alltt}
\hlcom{## Run GO analysis with weight algorithm}
\hlstd{goOutputWeight} \hlkwb{<-} \hlkwd{runGOAnalysis}\hlstd{(sigGenes,expGenes,goAnno,}
                                \hlkwc{pValThresh} \hlstd{=} \hlnum{0.05}\hlstd{,} \hlkwc{plotGO}\hlstd{ = }\hlnum{TRUE}\hlstd{,}
                                \hlkwc{algorithm}\hlstd{ = }\hlstr{"weight"}\hlstd{)}
\hlkwd{head}\hlstd{(goOutputWeight)}
\end{alltt}\end{kframe}\end{knitrout}


You can flip back and forth between the plots using the blue arrows
at the top of the plot device.
%
Doing this, you should be able to quickly see the difference between
the two algorithms.
%
You can also view the full data sets to see that many more GO terms
were included in the classic approach,
though they may not be more informative.


A few notes on significance testing are needed here.
%
The authors of the weight algorithm argue that their approach
implicitly accounts for both multiple testing and the dependency of the GO graph,
and so argue that the returned p-value does not need correcting.
%
It is up to you, and your research question,
to decide where to draw the threshold for significance.
%
As with the GO annotations themselves,
the trick is to find the right balance of conservative approach
to neither over- nor under- interpret your data.







%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Where to go for help} %%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

For more information on what is possible in \verb;topGO;
check out the help pages,
or visit their website (listed in further reading).
%
In addition, check out statistics sources
(textbooks, professor, wikipedia, etc.)
for more information on the underlying tests being performed.


%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{Further Reading} %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

More information on the \verb;topGO; package,
including the full package vignette
and a link to the full paper
(available open access at the link below)
can be found at:
\begin{itemize}
%  \item \url{http://cufflinks.cbcb.umd.edu/ }
%  \item \url{http://cufflinks.cbcb.umd.edu/manual.html#cuffmerge}
 \item \url{http://www.bioconductor.org/packages/release/bioc/html/topGO.html}
 \item \url{http://bioinformatics.oxfordjournals.org/content/22/13/1600.short}

\end{itemize}


% \begin{knitrout}\begin{kframe}\begin{alltt}
% code here
% \end{alltt}\end{kframe}\end{knitrout}
